import React from 'react'
import CardItem from './CartItem'
import './Cards.css'

function Cards() {
    return (
        <div className='cards'>
            <h1>Check out there EPIC Destinations!</h1>
            <div className='cards__container'>
                <div className='cards__wrapper'>
                    <ul className='cards__items'>
                        <CardItem 
                        src='images/img-9.jpg' text='Expolre the hidden waterfall' 
                        label='Adventure' path='/services'/>
                        <CardItem 
                        src='images/img-2.jpg' text='Expolre the hidden waterfall2' 
                        label='Adventure2' path='/services'/>
                    </ul>
                    
                    <ul className='cards__items'>
                        <CardItem 
                        src='images/img-9.jpg' text='Expolre the hidden waterfall' 
                        label='Adventure' path='/services'/>
                        <CardItem 
                        src='images/img-2.jpg' text='Expolre the hidden waterfall2' 
                        label='Adventure2' path='/services'/>
                        <CardItem 
                        src='images/img-2.jpg' text='Expolre the hidden waterfall2' 
                        label='Adventure2' path='/services'/>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards
